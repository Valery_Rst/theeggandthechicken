package net.vvoli.theEggOrTheChicken;

public class MainDemo {
    public static void main(String[] args) {
        TheEggOrTheChicken chicken = new TheEggOrTheChicken("Курица");
        TheEggOrTheChicken egg = new TheEggOrTheChicken("Яйцо");

        System.out.println("Спор начат!");
        chicken.start();
        egg.start();

        if (chicken.isAlive() || egg.isAlive()) {
            try{
                chicken.join();
                egg.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Первым появилось(ась) " + TheEggOrTheChicken.isName.get(1));
        }
    }
}
